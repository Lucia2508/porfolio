package com.example.porfolio.repository;

import com.example.porfolio.model.Servicios;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiciosRepository extends JpaRepository<Servicios, Long> {
}
