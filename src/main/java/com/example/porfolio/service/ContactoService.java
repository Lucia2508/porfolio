package com.example.porfolio.service;

import com.example.porfolio.model.Contacto;
import com.example.porfolio.repository.ContactoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContactoService {
    @Autowired
    private ContactoRepository contactoRepository;
    public List<Contacto> getAllContactos(){
        return contactoRepository.findAll();
    }

    public Contacto getContactoById(Long idContacto){
        return contactoRepository.findById(idContacto).orElse(null);
    }

    public Contacto addContacto(Contacto contacto){
        return contactoRepository.save(contacto);
    }
}
