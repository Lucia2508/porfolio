package com.example.porfolio.service;

import com.example.porfolio.model.Proyectos;
import com.example.porfolio.repository.ProyectosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProyectosService {
    @Autowired
    private ProyectosRepository proyectosRepository;
    public List<Proyectos> getAllProyectos(){
        return proyectosRepository.findAll();
    }
    public Proyectos getProyectosById(Long idProyectos){
        return proyectosRepository.findById(idProyectos).orElse(null);
    }
}
