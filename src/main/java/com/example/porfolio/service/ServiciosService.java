package com.example.porfolio.service;


import com.example.porfolio.model.Servicios;
import com.example.porfolio.repository.ServiciosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiciosService {
    @Autowired
    private ServiciosRepository serviciosRepository;
    public List<Servicios> getAllServicios(){
        return serviciosRepository.findAll();
    }
    public Servicios getServicioById(Long idServicios){
        return serviciosRepository.findById(idServicios).orElse(null);
    }
}
