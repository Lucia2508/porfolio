package com.example.porfolio.model;


import jakarta.persistence.*;

@Entity
@Table(name="informacionpersonal")
public class InformacionPersonal {

    @Id
    private Long id;

    private String nombre;
     private String foto_logo;
     private String texto;
     private String foto;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFoto_logo() {
        return foto_logo;
    }

    public void setFoto_logo(String foto_logo) {
        this.foto_logo = foto_logo;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
