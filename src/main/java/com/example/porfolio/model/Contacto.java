package com.example.porfolio.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="contacto")
public class Contacto {

    @Id
    private Long idContacto;
    private String nombre;
    private String correo;
    private String mensaje;

    public void setIdContacto(Long idContacto) {
        this.idContacto = idContacto;
    }

    public Long getIdContacto() {
        return idContacto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
