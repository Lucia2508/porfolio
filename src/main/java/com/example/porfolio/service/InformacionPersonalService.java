package com.example.porfolio.service;
import com.example.porfolio.model.InformacionPersonal;
import com.example.porfolio.repository.InformacionPersonalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InformacionPersonalService {
    @Autowired
    private InformacionPersonalRepository informacionPersonalRepository;
    public List<InformacionPersonal> getAllInformacionPersonal(){
        return informacionPersonalRepository.findAll();
    }
    public InformacionPersonal  getInformacionPersonalById(Long DNI){
        return informacionPersonalRepository.findById(DNI).orElse(null);
    }


}
