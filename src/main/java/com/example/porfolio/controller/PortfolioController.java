package com.example.porfolio.controller;

import com.example.porfolio.model.Contacto;
import com.example.porfolio.model.InformacionPersonal;
import com.example.porfolio.model.Proyectos;
import com.example.porfolio.model.Servicios;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.example.porfolio.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;


import java.util.List;

import java.util.List;

@Controller
public class PortfolioController {
    @Autowired
    private ContactoService contactoService;
    @Autowired
    private InformacionPersonalService informacionPersonalService;
    @Autowired
    private ProyectosService proyectosService;
    @Autowired
    private ServiciosService serviciosService;
    @GetMapping("/")
    String vertodo(Model model) {
        List<InformacionPersonal> descripcionList = informacionPersonalService.getAllInformacionPersonal();
        model.addAttribute("descripciones",descripcionList);

        List<Proyectos> proyectosList = proyectosService.getAllProyectos();
        model.addAttribute("proyectos",proyectosList);

        List<Servicios> serviciosList = serviciosService.getAllServicios();
        model.addAttribute("servicios",serviciosList);

        return "index";
    }

    @PostMapping("/")
    String todo( @RequestParam String nombre,@RequestParam String correo, @RequestParam String mensaje, Model model) {
        Contacto contact=new Contacto();
        contact.setNombre(nombre);
        contact.setCorreo(correo);
        contact.setMensaje(mensaje);

        contactoService.addContacto(contact);

        List<InformacionPersonal> descripcionList = informacionPersonalService.getAllInformacionPersonal();
        model.addAttribute("descripciones",descripcionList);

        List<Proyectos> proyectosList = proyectosService.getAllProyectos();
        model.addAttribute("proyectos",proyectosList);

        List<Servicios> serviciosList = serviciosService.getAllServicios();
        model.addAttribute("servicios",serviciosList);

        return "index";
    }
}

