package com.example.porfolio.repository;

import com.example.porfolio.model.InformacionPersonal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InformacionPersonalRepository extends JpaRepository<InformacionPersonal, Long> {
}
