package com.example.porfolio.repository;

import com.example.porfolio.model.Proyectos;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProyectosRepository extends JpaRepository<Proyectos, Long> {
}
