
drop database Portfolio;
create database if not exists Portfolio;
use Portfolio;

create table if not exists informacionPersonal(
id int primary key,
foto_logo varchar(255),
nombre varchar(255),
texto text,
foto varchar(255)

);
insert into informacionPersonal values (1, './img/logo.png','Soy Lucia, programadora',"Lenguajes: -Java -Html -CSS",'./img/foto2.jpg');





create table if not exists servicios(
idServicios int primary key,
nombre varchar(50)
);

insert  into servicios values (1, 'Desarrollo de Software');
insert  into servicios values (2, 'Diseño Web');
insert  into servicios values (3, 'Desarrollo de Aplicaciones Moviles');
insert  into servicios values (4, 'Analisis de datos');
insert  into servicios values (5, 'Seguridad informatica');
insert  into servicios values (6, 'Integracion de Sistemas');


create table if not exists proyectos(
idProyecto int primary key,
foto varchar(250)
);
insert into proyectos values(1, './img/diseño.jpg');
insert into proyectos values(2, './img/MicrosoftTeams-image.png');

create table if not exists Contacto(
idContacto int primary key,
nombre varchar(50),
correo varchar(50),
mensaje text
);



