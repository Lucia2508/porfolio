package com.example.porfolio.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="servicios")
public class Servicios {

    @Id
    private Long idServicios;
    private String nombre;


    public void setIdServicios(Long idServicios) {
        this.idServicios = idServicios;
    }

    public Long getIdServicios() {
        return idServicios;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
